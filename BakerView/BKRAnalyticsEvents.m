//
//  BakerAnalyticsEvents.m
//  Baker
//
//  ==========================================================================================
//
//  Copyright (c) 2010-2013, Davide Casali, Marco Colombo, Alessandro Morandi
//  Copyright (c) 2014, Andrew Krowczyk, Cédric Mériau, Pieter Claerhout
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are
//  permitted provided that the following conditions are met:
//
//  Redistributions of source code must retain the above copyright notice, this list of
//  conditions and the following disclaimer.
//  Redistributions in binary form must reproduce the above copyright notice, this list of
//  conditions and the following disclaimer in the documentation and/or other materials
//  provided with the distribution.
//  Neither the name of the Baker Framework nor the names of its contributors may be used to
//  endorse or promote products derived from this software without specific prior written
//  permission.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
//  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
//  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "BKRAnalyticsEvents.h"
#import "BKRSettings.h"

@implementation BKRAnalyticsEvents

#pragma mark - Singleton

+ (BKRAnalyticsEvents *)sharedInstance {
    static dispatch_once_t once;
    static BKRAnalyticsEvents *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {

        if ( [[BKRSettings sharedSettings].googleAnalyticsID length] > 0) {
            
            tracker = [[GAI sharedInstance] trackerWithTrackingId:[BKRSettings sharedSettings].googleAnalyticsID];
            // ****** Register to handle events
            [self registerEvents];
            
        }
    }
    return self;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Events

- (void)registerEvents {

    // Register the analytics event that are going to be tracked by Baker.
    NSArray *analyticEvents = @[@"BakerApplicationStart",
                               @"BakerIssueDownload",
                               @"BakerIssueOpen",
                               @"BakerIssueClose",
                               @"BakerIssuePurchase",
                               @"BakerIssueArchive",
                               @"BakerSubscriptionPurchase",
                               @"BakerViewPage",
                               @"BakerViewIndexOpen",
                               @"BakerViewModalBrowser"];
    
    for (NSString *eventName in analyticEvents) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveEvent:)
                                                     name:eventName
                                                   object:nil];
    }
    
}

- (void)receiveEvent:(NSNotification*)notification {
    //NSLog(@"[BakerAnalyticsEvent] Received event %@", notification.name); // Uncomment this to debug
    //GAI Activation
    //id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    // If you want, you can handle differently the various events
    if ([[notification name] isEqualToString:@"BakerApplicationStart"]) {
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Magazine"     // Event category (required)
                                                              action:@"Magazine Launched"  // Event action (required)
                                                               label:@"Magazine Launched"          // Event label
                                                               value:nil] build]];    // Event value
        
    } else if ([[notification name] isEqualToString:@"BakerIssueDownload"]) {
        // Track here when a issue download is requested
        BKRIssueCell *issueCell = [notification object];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Issue"     // Event category (required)
                                                              action:@"Issue Downloaded"  // Event action (required)
                                                               label:[NSString stringWithFormat: @"%@", issueCell.issue.ID]   // Event label
                                                               value:nil] build]];    // Event value
        
    } else if ([[notification name] isEqualToString:@"BakerIssueOpen"]) {
        // Track here when a issue is opened to be read
        BKRIssueCell *issueCell = [notification object];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Issue"     // Event category (required)
                                                              action:@"Issue Opened"  // Event action (required)
                                                               label:[NSString stringWithFormat: @"%@", issueCell.issue.ID]
                                                               value:nil] build]];    // Event value
        
    } else if ([[notification name] isEqualToString:@"BakerIssueClose"]) {
        // Track here when a issue that was being read is closed
        BKRBookViewController *bakerview = [notification object];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Issue"     // Event category (required)
                                                              action:@"Issue Closed"  // Event action (required)
                                                               label: [NSString stringWithFormat: @"%@", bakerview.book.title]
                                                               value:nil] build]];    // Event value
        
    } else if ([[notification name] isEqualToString:@"BakerIssuePurchase"]) {
        // Track here when a issue purchase is requested
        BKRIssueCell *issueCell = [notification object];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Purchase Actions"     // Event category (required)
                                                              action:@"Purchase Button Clicked"  // Event action (required)
                                                               label:[NSString stringWithFormat: @"%@",issueCell.issue.ID]    // Event label
                                                               value:nil] build]];    // Event value
        
    } else if ([[notification name] isEqualToString:@"BakerIssueArchive"]) {
        // Track here when a issue archival is requested
        BKRIssueCell *issueCell = [notification object];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Issue"     // Event category (required)
                                                              action:@"Archive Issue Clicked"  // Event action (required)
                                                               label:[NSString stringWithFormat: @"%@",issueCell.issue.ID]   // Event label
                                                               value:nil] build]];    // Event value
        
    } else if ([[notification name] isEqualToString:@"BakerSubscriptionPurchase"]) {
        // Track here when a subscription purchased is requested
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Purchase Actions"     // Event category (required)
                                                              action:@"Subscription Button Clicked"  // Event action (required)
                                                               label:@"Subscription Button Clicked"          // Event label
                                                               value:nil] build]];    // Event value
        
    } else if ([[notification name] isEqualToString:@"BakerViewPage"]) {
        // Track here when a specific page is opened
        BKRBookViewController *bakerview = [notification object];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Issue"
                                                              action: @"Page Views"
                                                               label: [NSString stringWithFormat: @"%@: %@", bakerview.book.title, [bakerview.currPage stringByEvaluatingJavaScriptFromString:@"document.title"]]
                                                               value:nil] build]];
        
    } else if ([[notification name] isEqualToString:@"BakerViewIndexOpen"]) {
        // Track here the opening of the index and status bar
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Issue"     // Event category (required)
                                                              action:@"Index Pageviews"  // Event action (required)
                                                               label:@"Index Pageviews"
                                                               value:nil] build]];    // Event value
        
    } else if ([[notification name] isEqualToString:@"BakerViewModalBrowser"]) {

        // Track here the opening of the modal view
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Issue"     // Event category (required)
                                                              action:@"Modal View Open"  // Event action (required)
                                                               label:@"Modal View Open"          // Event label
                                                               value:nil] build]];    // Event value
    } else {
    }

}

@end
