## 1.1.0 (2015-07-11)

Features:

  - Improved Shelf Design

Bugfixes:

  - Fix shelf layout issues when rotating the device

## 1.0.0 (2015-06-01)

Features:

  - Initial tracked version of MagLoft

Bugfixes:

  - Very much bugfix!
